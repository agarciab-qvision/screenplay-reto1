package co.com.proyectobase.screenplay.tasks;



import java.util.List;

import org.apache.bcel.generic.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import co.com.proyectobase.screenplay.ui.WebAutomationDemoSitePage;
import cucumber.api.java.bs.Dato;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.questions.Value;

public class Diligenciar implements Task {
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Enter.theValue("Alexander").into(WebAutomationDemoSitePage.CAMPO_NOMBRES));
		actor.attemptsTo(Enter.theValue("Garcia Berrio").into(WebAutomationDemoSitePage.CAMPO_APELLIDOS));
		actor.attemptsTo(Enter.theValue("CEOH").into(WebAutomationDemoSitePage.CAMPO_DIRECCION));
		actor.attemptsTo(Enter.theValue("agarciab@choucairtesting.com").into(WebAutomationDemoSitePage.CAMPO_CORREO_ELECTRONICO));
		actor.attemptsTo(Enter.theValue("3005967159").into(WebAutomationDemoSitePage.CAMPO_TELEFONO));
		actor.attemptsTo(Click.on(WebAutomationDemoSitePage.OPCION_SEXO_MASCULINO));
		actor.attemptsTo(Click.on(WebAutomationDemoSitePage.HOBBIE2));
		actor.attemptsTo(Click.on(WebAutomationDemoSitePage.LENGUAJE));
		listaSeleccionarLenguaje(WebAutomationDemoSitePage.LENGUAJE2.resolveFor(actor), "Spanish");
		actor.attemptsTo(SelectFromOptions.byVisibleText("Android").from(WebAutomationDemoSitePage.SKILLS));
		actor.attemptsTo(SelectFromOptions.byVisibleText("Colombia").from(WebAutomationDemoSitePage.PAIS));
		actor.attemptsTo(SelectFromOptions.byVisibleText("Australia").from(WebAutomationDemoSitePage.PAIS2));
		actor.attemptsTo(Click.on(WebAutomationDemoSitePage.LISTA_ANO_NACIMIENTO));
		actor.attemptsTo(SelectFromOptions.byVisibleText("1978").from(WebAutomationDemoSitePage.LISTA_ANO_NACIMIENTO));
		actor.attemptsTo(Click.on(WebAutomationDemoSitePage.LISTA_MES_NACIMIENTO));
		actor.attemptsTo(SelectFromOptions.byVisibleText("October").from(WebAutomationDemoSitePage.LISTA_MES_NACIMIENTO));
		actor.attemptsTo(Click.on(WebAutomationDemoSitePage.LISTA_DIA_NACIMIENTO));
		actor.attemptsTo(SelectFromOptions.byVisibleText("9").from(WebAutomationDemoSitePage.LISTA_DIA_NACIMIENTO));
		actor.attemptsTo(Enter.theValue("Clavetempor@l1").into(WebAutomationDemoSitePage.CAMPO_CONTRASEÑA));
		actor.attemptsTo(Enter.theValue("Clavetempor@l1").into(WebAutomationDemoSitePage.CAMPO_CONFIRMACION_CONTRASEÑA));
		actor.attemptsTo(Click.on(WebAutomationDemoSitePage.BOTON_ENVIAR));
	}

	public static Diligenciar FormularioDeRegistro() {
		return Tasks.instrumented(Diligenciar.class);
	}
	

	public void listaSeleccionarLenguaje (WebElementFacade objeto, String lenguaje){
		List<WebElement> listaLenguajes = objeto.findElements(By.tagName("li"));
		
		for (WebElement e : listaLenguajes){
			if (e.getText().equals(lenguaje)){
				e.click();
				break;
			}
		}
	}

	
}
