package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")

public class WebAutomationDemoSitePage extends PageObject {

	public static final Target CAMPO_NOMBRES = Target.the("El campo donde se diligencian los nombres del usuario").located(By.xpath("//*[@id='basicBootstrapForm']/div[1]/div[1]/input"));
	public static final Target CAMPO_APELLIDOS = Target.the("El campo donde se diligencian los apellidos del usuario").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[1]/div[2]/input"));
	public static final Target CAMPO_DIRECCION = Target.the("Direccion del usuario").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[2]/div/textarea"));	
	public static final Target CAMPO_CORREO_ELECTRONICO = Target.the("El campo donde se diligencia la dirección de correo electrónico del usuario").located(By.xpath("//*[@id=\'eid\']/input"));
	public static final Target CAMPO_TELEFONO = Target.the("El campo donde se diligencia el número telefónico del usuario").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[4]/div/input"));
	public static final Target OPCION_SEXO_MASCULINO = Target.the("El botón donde se selecciona el sexo del usuario").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[5]/div/label[1]/input"));
	public static final Target HOBBIE2 = Target.the("El segundo Hobbie del usuario").located(By.id("checkbox2"));
	public static final Target LENGUAJE = Target.the("La lista de lenguaje del usuario").located(By.id("msdd"));
	public static final Target LENGUAJE2 = Target.the("El lenguaje que maneja el usuario").located(By.className("ui-corner-all"));
	public static final Target SKILLS = Target.the("Skills del usuario").located(By.id("Skills"));
	public static final Target PAIS = Target.the("La lista de donde se selecciona el país al que pertenece el usuario").located(By.id("countries"));
	public static final Target PAIS2 = Target.the("La lista de donde se selecciona el país al que pertenece el usuario").located(By.id("country"));
	public static final Target LISTA_ANO_NACIMIENTO = Target.the("La lista de donde se selecciona el año de nacimiento del usuario").located(By.id("yearbox"));
	public static final Target LISTA_MES_NACIMIENTO = Target.the("La lista de donde se selecciona el mes de nacimiento del usuario").located(By.xpath("//*[@id=\'basicBootstrapForm\']/div[11]/div[2]/select"));
	public static final Target LISTA_DIA_NACIMIENTO = Target.the("La lista de donde se selecciona el día de nacimiento del usuario").located(By.id("daybox"));
	public static final Target CAMPO_CONTRASEÑA = Target.the("El campo donde se diligencia la contraseña asignada por el usuario").located(By.id("firstpassword"));
	public static final Target CAMPO_CONFIRMACION_CONTRASEÑA = Target.the("El campo donde se confirma la contraseña asignada por el usuario").located(By.id("secondpassword"));
	public static final Target BOTON_ENVIAR = Target.the("El botón para registrar los datos diligenciados por el usuario").located(By.id("submitbtn"));
	
}
