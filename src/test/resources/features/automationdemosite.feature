#Author: agarciab@choucairtesting.com
#language:es

@tag
Característica: Automation Demo Site
  Como usuario
  Quiero ingresar al sitio Web Automation Demo Site
  A registrar mi usuario

  @tag1
  Escenario: Registrar usuario
    Dado que Carlos quiere acceder a la Web Automation Demo Site
    Cuando el realiza el registro en la pagina
    Entonces el verifica que se carga la pantalla con texto Double Click on Edit Icon to EDIT the Table Row