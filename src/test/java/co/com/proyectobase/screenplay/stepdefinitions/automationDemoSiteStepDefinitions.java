package co.com.proyectobase.screenplay.stepdefinitions;


import static org.hamcrest.Matchers.equalTo;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

import org.hamcrest.Matcher;
import org.openqa.selenium.WebDriver;
import co.com.proyectobase.screenplay.questions.LaPantalla;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Diligenciar;
import cucumber.api.java.Before;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class automationDemoSiteStepDefinitions {

	@Managed(driver="chrome")
    private WebDriver hisBrowser;
    private Actor carlos = Actor.named("Carlos");
    
    @Before
    public void configuracionInicial() {
    	carlos.can(BrowseTheWeb.with(hisBrowser));
    }
	
	@Dado("^que Carlos quiere acceder a la Web Automation Demo Site$")
	public void queCarlosQuiereAccederALaWebAutomationDemoSite()  {
		carlos.wasAbleTo(Abrir.LaPaginaDeAutomationDemoSite());
	}


	@Cuando("^el realiza el registro en la pagina$")
	public void elRealizaElRegistroEnLaPagina()  {
		carlos.attemptsTo(Diligenciar.FormularioDeRegistro());
	}

	@Entonces("^el verifica que se carga la pantalla con texto Double Click on Edit Icon to EDIT the Table Row$")
	public void elVerificaQueSeCargaLaPantallaConTextoDoubleClickOnEditIconToEDITTheTableRow()  {
		carlos.should(seeThat(LaPantalla.muestra(), equalTo("- Double Click on Edit Icon to EDIT the Table Row.")));
	}
	
}
